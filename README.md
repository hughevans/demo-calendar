# Demo Calendar

<http://glass-maker-aaron-78767.netlify.com/>

A basic React calendar component with some simple state management.

## Install

```
yarn
```

## Development

```
yarn run start
```

## Tests

```
yarn run test
```

## Deployment

```
yarn run build
netlify deploy
```
