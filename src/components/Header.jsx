import React from 'react';
import PropTypes from 'prop-types';

import Controls from './Controls';

import './Header.css';

const Header = ({ date, setNewDate }) => {
  const monthName = date.toLocaleString('lookup', { month: 'long' });

  return (
    <div className="header">
      <h1>{monthName}</h1>
      <Controls date={date} setNewDate={setNewDate} />
    </div>
  );
};

Header.propTypes = {
  date: PropTypes.instanceOf(Date),
  setNewDate: PropTypes.func,
};

Header.defaultProps = {
  date: new Date(),
  setNewDate: () => {},
};

export default Header;
