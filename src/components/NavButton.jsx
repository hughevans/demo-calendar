import React from 'react';
import PropTypes from 'prop-types';

import './NavButton.css';

function directionText(direction) {
  return direction === 'PREV' ? '←' : '→';
}

const NavButton = ({ direction, onClick }) => {
  const handleClick = (event) => {
    event.preventDefault();
    onClick(direction);
  };

  return (
    <button className="nav-button" onClick={handleClick}>
      {directionText(direction)}
    </button>
  );
};

NavButton.propTypes = {
  direction: PropTypes.string,
  onClick: PropTypes.func,
};

NavButton.defaultProps = {
  direction: '',
  onClick: () => {},
};

export default NavButton;
