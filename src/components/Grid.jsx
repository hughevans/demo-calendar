import React from 'react';
import PropTypes from 'prop-types';

import { datesInMonth, overlapDates } from './../dates';
import Cell from './Cell';

import './Grid.css';

function cells(dates, isPreviousMonth = false) {
  return dates.map(date => <Cell date={date} isPreviousMonth={isPreviousMonth} key={date} />);
}

const Grid = ({ date }) => (
  <div className="grid">
    {cells(overlapDates(date), true)}
    {cells(datesInMonth(date))}
  </div>
);

Grid.propTypes = {
  date: PropTypes.instanceOf(Date),
};

Grid.defaultProps = {
  date: new Date(),
};

export default Grid;
