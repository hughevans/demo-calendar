import React from 'react';
import PropTypes from 'prop-types';

import Header from './Header';
import Grid from './Grid';

import './Calendar.css';

const Calendar = ({ date, setNewDate }) => (
  <div className="calendar">
    <Header date={date} setNewDate={setNewDate} />
    <Grid date={date} />
  </div>
);

Calendar.propTypes = {
  date: PropTypes.instanceOf(Date),
  setNewDate: PropTypes.func,
};

Calendar.defaultProps = {
  date: new Date(),
  setNewDate: () => {},
};

export default Calendar;
