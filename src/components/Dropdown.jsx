import React from 'react';
import PropTypes from 'prop-types';

function options(optionData) {
  return optionData.map(option => (
    <option value={option.value} key={option.value}>
      {option.label}
    </option>
  ));
}

const Dropdown = ({ onSelect, optionData, selected }) => {
  const handleChange = (event) => {
    event.preventDefault();
    onSelect(event.target.value);
  };

  return (
    <select value={selected} onChange={handleChange}>
      {options(optionData)}
    </select>
  );
};

Dropdown.propTypes = {
  onSelect: PropTypes.func,
  optionData: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string,
      value: PropTypes.string,
    }),
  ).isRequired,
  selected: PropTypes.string.isRequired,
};

Dropdown.defaultProps = {
  onSelect: () => {},
};

export default Dropdown;
