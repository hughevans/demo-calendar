import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import { isToday } from './../dates';

import './Cell.css';

const Cell = ({ date, isPreviousMonth }) => {
  const dateInMonth = date.getDate();

  const cellClass = classNames({
    cell: true,
    'from-previous-month': isPreviousMonth,
  });

  const dateClass = classNames({
    date: true,
    today: isToday(date),
  });

  return (
    <div className={cellClass}>
      <div className={dateClass}>{dateInMonth}</div>
    </div>
  );
};

Cell.propTypes = {
  date: PropTypes.instanceOf(Date).isRequired,
  isPreviousMonth: PropTypes.bool,
};

Cell.defaultProps = {
  isPreviousMonth: true,
};

export default Cell;
