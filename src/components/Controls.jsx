import React from 'react';
import PropTypes from 'prop-types';

import Dropdown from './Dropdown';
import NavButton from './NavButton';

import './Controls.css';

function monthOptionData() {
  return Array.from(Array(12), (_v, i) => {
    const date = new Date(2000, i, 1);
    return {
      value: i.toString(),
      label: date.toLocaleString('lookup', { month: 'long' }),
    };
  });
}

function yearOptionData(pastYearRange, futureYearRange) {
  const thisYear = new Date().getFullYear();
  const firstYear = thisYear - pastYearRange;
  const yearRange = pastYearRange + futureYearRange + 1;

  return Array.from(Array(yearRange), (_v, i) => {
    const year = firstYear + i;
    return {
      value: year.toString(),
      label: year.toString(),
    };
  });
}

const Controls = ({ date, futureYearRange, pastYearRange, setNewDate }) => {
  const handleMonthSelect = (month) => {
    const newDate = new Date(date.getFullYear(), month, 1);
    setNewDate(newDate);
  };

  const handleYearSelect = (year) => {
    const newDate = new Date(year, date.getMonth(), 1);
    setNewDate(newDate);
  };

  const handlePrevMonth = () => {
    const newDate = new Date(date.getFullYear(), date.getMonth() - 1, 1);
    setNewDate(newDate);
  };

  const handleNextMonth = () => {
    const newDate = new Date(date.getFullYear(), date.getMonth() + 1, 1);
    setNewDate(newDate);
  };

  return (
    <form className="controls">
      <NavButton direction="PREV" onClick={handlePrevMonth} />
      <Dropdown
        optionData={monthOptionData()}
        onSelect={handleMonthSelect}
        selected={date.getMonth().toString()}
      />
      <Dropdown
        optionData={yearOptionData(pastYearRange, futureYearRange)}
        onSelect={handleYearSelect}
        selected={date.getFullYear().toString()}
      />
      <NavButton direction="NEXT" onClick={handleNextMonth} />
    </form>
  );
};

Controls.propTypes = {
  date: PropTypes.instanceOf(Date),
  futureYearRange: PropTypes.number,
  pastYearRange: PropTypes.number,
  setNewDate: PropTypes.func,
};

Controls.defaultProps = {
  date: new Date(),
  futureYearRange: 7,
  pastYearRange: 7,
  setNewDate: () => {},
};

export default Controls;
