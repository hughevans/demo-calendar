import React, { Component } from 'react';

import Calendar from './components/Calendar';

function setDateHash(date) {
  const dateString = `${date.getFullYear()}-${date.getMonth() + 1}`;
  const baseUri = `${window.location.pathname}${window.location.search}`;

  window.location.replace(`${baseUri}#${dateString}`);
}

class App extends Component {
  constructor(props) {
    super(props);
    this.navigated = this.navigated.bind(this);

    this.state = {
      initializing: true,
    };
  }

  componentDidMount() {
    window.addEventListener('hashchange', this.navigated, false);
    this.navigated();
  }

  componentWillUnmount() {
    window.removeEventListener('hashchange', this.navigated, false);
  }

  navigated() {
    const pattern = /^#{1}(\d{4})-(\d{1,2})$/;
    const dateParts = window.location.hash.match(pattern);

    if (dateParts) {
      const year = parseInt(dateParts[1], 10);
      const month = parseInt(dateParts[2], 10) - 1;
      const newDate = new Date(year, month, 1);

      this.setState({
        initializing: false,
        date: newDate,
      });
    } else {
      setDateHash(new Date());
    }
  }

  render() {
    if (this.state.initializing) {
      return <div />;
    }

    return <Calendar date={this.state.date} setNewDate={setDateHash} />;
  }
}

export default App;
