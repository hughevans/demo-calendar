export function lastDateOfMonth(date) {
  return new Date(date.getFullYear(), date.getMonth() + 1, 0);
}

export function daysInMonth(date) {
  return Array.from(Array(lastDateOfMonth(date).getDate()), (_v, i) => i + 1);
}

export function datesInMonth(date) {
  return daysInMonth(date).map(day => new Date(date.getFullYear(), date.getMonth(), day));
}

export function firstDateOfMonth(date) {
  return new Date(date.getFullYear(), date.getMonth(), 1);
}

export function overlapDates(date) {
  const daysVisible = firstDateOfMonth(date).getDay();
  const maxDaysBack = daysVisible * -1;
  return Array.from(
    Array(daysVisible),
    (_v, i) => new Date(date.getFullYear(), date.getMonth(), maxDaysBack + 1 + i),
  );
}

export function isToday(date) {
  const now = new Date();

  return (
    now.getFullYear() === date.getFullYear() &&
    now.getMonth() === date.getMonth() &&
    now.getDate() === date.getDate()
  );
}
